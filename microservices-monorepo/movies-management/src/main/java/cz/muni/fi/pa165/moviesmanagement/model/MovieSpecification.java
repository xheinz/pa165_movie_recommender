package cz.muni.fi.pa165.moviesmanagement.model;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import lombok.experimental.UtilityClass;
import org.springframework.data.jpa.domain.Specification;

import java.util.Set;

@UtilityClass
public class MovieSpecification {

    public static Specification<Movie> hasId(Long id) {
        return (root, query, cb) -> id == null ? null : cb.equal(root.get("id"), id);
    }

    public static Specification<Movie> hasName(String name) {
        return (root, query, cb) -> name == null ? null : cb.like(root.get("name"), "%" + name + "%");
    }

    public static Specification<Movie> hasGenre(Set<MovieGenre> genres) {
        return (root, query, cb) -> {
            if (genres == null || genres.isEmpty()) {
                return null;
            }
            return root.join("genres").in(genres);
        };
    }

    public static Specification<Movie> buildSearchSpecification(Long id, String name, Set<MovieGenre> genres) {
        return Specification.where(hasId(id))
                .and(hasName(name))
                .and(hasGenre(genres));
    }
}
