package cz.muni.fi.pa165.moviesmanagement.config;

import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.repository.MovieRepository;
import cz.muni.fi.pa165.moviesmanagement.utils.MovieMockDataFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.List;

@Configuration
public class DatabaseInitializer {

    /**
     * Initializes the database with some movies.
     *
     * @param repository The movie repository.
     * @return The command line runner.
     */
    @Bean
    @Profile("db-init")
    public CommandLineRunner initDatabase(final MovieRepository repository) {
        return args -> {
            List<Movie> movies = MovieMockDataFactory.createAllMockMoviesWithoutId();
            repository.saveAll(movies);
        };
    }

    /**
     * Clears all data from the movies database.
     *
     * @param repository The movie repository used for data operations.
     * @return The command line runner that clears the database.
     */
    @Bean
    @Profile("db-clear")
    public CommandLineRunner clearDatabase(final MovieRepository repository) {
        return args -> repository.deleteAll();
    }
}
