package cz.muni.fi.pa165.moviesmanagement.dto;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

import java.util.List;
import java.util.Set;

public record CreateMovieDto(@NotBlank(message = "Name cannot be empty") String name,
                             @NotBlank(message = "Director cannot be empty") String director,
                             @NotBlank(message = "Description cannot be empty") String description,

                             @NotEmpty(message = "Genres cannot be empty") Set<MovieGenre> genres,
                             String imageUrl,
                             @NotEmpty(message = "At least one actor must be provided") List<String> actors) {
}
