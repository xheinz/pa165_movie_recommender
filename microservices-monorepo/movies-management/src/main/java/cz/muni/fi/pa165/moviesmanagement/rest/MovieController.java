package cz.muni.fi.pa165.moviesmanagement.rest;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.facade.MovieFacade;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Optional;

@RestController
@RequestMapping("/api/movies")
@Tag(name = "Movies", description = "Endpoints for movies management")
@CrossOrigin(origins = "*")
public class MovieController {

    private final MovieFacade movieFacade;

    /**
     * Constructor for MovieController.
     *
     * @param movieFacade The movie facade.
     */
    @Autowired
    public MovieController(final MovieFacade movieFacade) {
        this.movieFacade = movieFacade;
    }

    /**
     * Search movies dynamically based on parameters.
     *
     * @param id    Optional identifier of the movie.
     * @param name  Optional name of the movie.
     * @param genre Optional genre of the movie.
     * @return A ResponseEntity containing the filtered list of movies.
     */
    @GetMapping("/find")
    public ResponseEntity<Collection<MovieDto>> find(
            @RequestParam(required = false) Long id,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) MovieGenre genre) {
        Collection<MovieDto> movies = movieFacade.find(id, name, genre);
        return movies.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok(movies);
    }

    /**
     * Add movie
     *
     * @param movieDto to be added
     * @return added movie
     */
    @PostMapping
    public ResponseEntity<MovieDto> addMovie(@RequestBody final CreateMovieDto movieDto) {
        MovieDto createdMovie = movieFacade.addMovie(movieDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdMovie);
    }

    /**
     * Update movie
     *
     * @param movieDto to be updated
     * @return updated movie
     */
    @PutMapping
    public ResponseEntity<MovieDto> updateMovie(@RequestBody final MovieDto movieDto) {
        Optional<MovieDto> updatedMovie = Optional.ofNullable(movieFacade.updateMovie(movieDto));
        return updatedMovie.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Delete movie
     *
     * @param id of movie to be deleted
     * @return Boolean true if movie was deleted, false otherwise
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteMovie(@PathVariable final Long id) {
        var deleted = movieFacade.deleteMovie(id);
        return deleted ? ResponseEntity.ok(true) : ResponseEntity.notFound().build();
    }

    /**
     * Get users favorite movies
     *
     * @param userId of user
     * @return collection of users favorite movies
     */
    @GetMapping("/user/{userId}/favorite")
    public ResponseEntity<Collection<MovieDto>> getUsersFavoriteMovies(@PathVariable final Long userId) {
        return ResponseEntity.ok(movieFacade.getUsersFavoriteMovies(userId));
    }
}
