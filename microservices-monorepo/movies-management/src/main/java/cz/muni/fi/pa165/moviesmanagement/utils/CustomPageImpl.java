package cz.muni.fi.pa165.moviesmanagement.utils;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.List;

/**
 * Custom implementation of the PageImpl class.
 * Based on the tutorial *Consuming Page Entity Response*
 * obtained from <a href="https://www.baeldung.com/resttemplate-page-entity-response">Baeldung.com</a>
 *
 * @param <T> The type of the content.
 */

public class CustomPageImpl<T> extends PageImpl<T> {

    private static final int DEFAULT_PAGE_SIZE = 10;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public CustomPageImpl(@JsonProperty("content") List<T> content, @JsonProperty("number") int number,
                          @JsonProperty("size") int size, @JsonProperty("totalElements") Long totalElements,
                          @JsonProperty("pageable") JsonNode pageable, @JsonProperty("last") boolean last,
                          @JsonProperty("totalPages") int totalPages, @JsonProperty("sort") JsonNode sort,
                          @JsonProperty("numberOfElements") int numberOfElements) {
        super(content, PageRequest.of(number, 1), DEFAULT_PAGE_SIZE);
    }
}
