package cz.muni.fi.pa165.moviesmanagement.facade;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;

import java.util.Collection;
import java.util.List;

public interface MovieFacade {
    /**
     * Search movies dynamically based on given parameters.
     *
     * @param id    The movie's identifier, if provided.
     * @param name  The movie's name, if provided.
     * @param genre The movie's genre, if provided.
     * @return A list of movies that match the criteria.
     */
    Collection<MovieDto> find(Long id, String name, MovieGenre genre);

    /**
     * Adds a new movie to the repository.
     *
     * @param movieDto The movie DTO to be added.
     * @return The added movie DTO.
     */
    MovieDto addMovie(CreateMovieDto movieDto);

    /**
     * Deletes a movie by its identifier.
     *
     * @param id The identifier of the movie to be deleted.
     * @return true if the movie was successfully deleted, false otherwise.
     */
    Boolean deleteMovie(Long id);

    /**
     * Updates an existing movie.
     *
     * @param movieDto The movie DTO with updated fields.
     * @return The updated movie DTO.
     */
    MovieDto updateMovie(MovieDto movieDto);

    /**
     * Get users favorite movies
     *
     * @param userId of user
     * @return list of users favorite movies
     */
    List<MovieDto> getUsersFavoriteMovies(Long userId);
}
