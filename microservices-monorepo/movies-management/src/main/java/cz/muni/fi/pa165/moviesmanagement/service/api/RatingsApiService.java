package cz.muni.fi.pa165.moviesmanagement.service.api;

import cz.muni.fi.pa165.moviesmanagement.dto.ReviewDTO;
import cz.muni.fi.pa165.moviesmanagement.utils.CustomPageImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.List;

@Service
public class RatingsApiService {

    private final WebClient client;

    /**
     * Constructor for RatingsApiService.
     *
     * @param baseUrl The base URL of the ratings and reviews service.
     */
    public RatingsApiService(@Value("${service.ratings-and-reviews.url}") final String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    /**
     * Retrieves all reviews.
     *
     * @param userId The identifier of the movie.
     * @return A list of all review DTOs.
     */
    public List<ReviewDTO> getReviewsByUserId(final Long userId) {
        ParameterizedTypeReference<CustomPageImpl<ReviewDTO>> responseType = new ParameterizedTypeReference<>() {
        };

        Page<ReviewDTO> page = client.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/api/reviews/user/{userId}")
                        .build(userId))
                .retrieve()
                .bodyToMono(responseType)
                .block();

        if (page != null) {
            return page.getContent();
        } else {
            return new ArrayList<>();
        }
    }

}
