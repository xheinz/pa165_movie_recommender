package cz.muni.fi.pa165.moviesmanagement.utils;

import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.mapper.MovieMapper;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;

import java.util.List;
import java.util.Set;

public class MovieMockDataFactory {

    private MovieMockDataFactory() {}

    public static Movie createShawshankWithoutId() {
        Movie movie = new Movie();
        movie.setName("The Shawshank Redemption");
        movie.setDirector("Frank Darabont");
        movie.setDescription("i dont remember what it is about exactly");
        movie.setActors(List.of("Tim Robbins", "Morgan Freeman"));
        movie.setGenres(Set.of(MovieGenre.DRAMA));
        movie.setImageUrl("https://www.imdb.com/title/tt0111161/mediaviewer/rm10105600");
        return movie;
    }

    public static Movie createShawshankWithId(Long id) {
        Movie movie = createShawshankWithoutId();
        movie.setId(id);
        return movie;
    }

    public static Movie createGodfatherWithoutId() {
        Movie movie = new Movie();
        movie.setName("The Godfather");
        movie.setDirector("Francis Ford Coppola");
        movie.setDescription("i dont remember what it is about exactly");
        movie.setActors(List.of("Marlon Brando", "Al Pacino"));
        movie.setGenres(Set.of(MovieGenre.CRIME, MovieGenre.DRAMA));
        movie.setImageUrl("https://www.imdb.com/title/tt0068646/mediaviewer/rm10105600");
        return movie;
    }

    public static Movie createGodfatherWithId(Long id) {
        Movie movie = createGodfatherWithoutId();
        movie.setId(id);
        return movie;
    }

    public static Movie createBastardi1WithoutId() {
        Movie movie = new Movie();
        movie.setName("Bastardi 1");
        movie.setDirector("Tomas Magnusek");
        movie.setDescription("i dont remember what it is about exactly");
        movie.setActors(List.of("Tomas Magnusek"));
        movie.setGenres(Set.of(MovieGenre.DRAMA, MovieGenre.COMEDY));
        movie.setImageUrl("https://www.imdb.com/title/tt0111161/mediaviewer/rm10105600");
        return movie;
    }

    public static Movie createBastardi1WithId(Long id) {
        Movie movie = createBastardi1WithoutId();
        movie.setId(id);
        return movie;
    }

    public static Movie createBastardi2WithoutId() {
        Movie movie = new Movie();
        movie.setName("Bastardi 2");
        movie.setDirector("Tomas Magnusek");
        movie.setDescription("i dont remember what it is about exactly");
        movie.setActors(List.of("Tomas Magnusek"));
        movie.setGenres(Set.of(MovieGenre.DRAMA, MovieGenre.COMEDY));
        movie.setImageUrl("https://www.imdb.com/title/tt0111161/mediaviewer/rm10105600");
        return movie;
    }

    public static Movie createBastardi2WithId(Long id) {
        Movie movie = createBastardi2WithoutId();
        movie.setId(id);
        return movie;
    }

    public static Movie createBastardi3WithoutId() {
        Movie movie = new Movie();
        movie.setName("Bastardi 3");
        movie.setDirector("Tomas Magnusek");
        movie.setDescription("i dont remember what it is about exactly");
        movie.setActors(List.of("Tomas Magnusek"));
        movie.setGenres(Set.of(MovieGenre.DRAMA, MovieGenre.COMEDY));
        movie.setImageUrl("https://www.imdb.com/title/tt0111161/mediaviewer/rm10105600");
        return movie;
    }

    public static Movie createBastardi3WithId(Long id) {
        Movie movie = createBastardi3WithoutId();
        movie.setId(id);
        return movie;
    }

    public static Movie createBastardi4WithoutId() {
        Movie movie = new Movie();
        movie.setName("Bastardi 4: Reparát");
        movie.setDirector("Tomas Magnusek");
        movie.setDescription("i dont remember what it is about exactly");
        movie.setActors(List.of("Tomas Magnusek"));
        movie.setGenres(Set.of(MovieGenre.DRAMA, MovieGenre.COMEDY));
        movie.setImageUrl("https://www.imdb.com/title/tt0111161/mediaviewer/rm10105600");
        return movie;
    }

    public static Movie createBastardi4WithId(Long id) {
        Movie movie = createBastardi4WithoutId();
        movie.setId(id);
        return movie;
    }


    public static List<Movie> createAllMockMoviesWithoutId() {
        return List.of(
                createShawshankWithoutId(),
                createGodfatherWithoutId(),
                createBastardi1WithoutId(),
                createBastardi2WithoutId(),
                createBastardi3WithoutId(),
                createBastardi4WithoutId()
        );
    }


    public static List<Movie> createAllMockMoviesWithIds() {
        return List.of(
                createShawshankWithId(1L),
                createGodfatherWithId(2L),
                createBastardi1WithId(3L),
                createBastardi2WithId(4L),
                createBastardi3WithId(5L),
                createBastardi4WithId(6L)
        );
    }


    public static List<MovieDto> createAllMockMoviesWithoutIdAsDto() {
        MovieMapper mapper = new MovieMapper();
        List<Movie> movies = createAllMockMoviesWithoutId();
        return movies.stream().map(mapper::convertToDto).toList();
    }

    public static List<MovieDto> createAllMockMoviesWithIdsAsDto() {
        MovieMapper mapper = new MovieMapper();
        List<Movie> movies = createAllMockMoviesWithIds();
        return movies.stream().map(mapper::convertToDto).toList();
    }
}
