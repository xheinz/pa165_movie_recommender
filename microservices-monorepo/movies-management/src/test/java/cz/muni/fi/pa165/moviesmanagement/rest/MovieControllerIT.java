package cz.muni.fi.pa165.moviesmanagement.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.repository.MovieRepository;
import cz.muni.fi.pa165.moviesmanagement.utils.KeycloakTokenProvider;
import cz.muni.fi.pa165.moviesmanagement.utils.MovieMockDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class MovieControllerIT {

    @Autowired
    private MockMvc mockMvc;

    private String accessToken;

    @Autowired
    private KeycloakTokenProvider keycloakTokenProvider;

    @MockBean
    private MovieRepository movieRepository;

    @BeforeEach
    public void setup() {
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }


    @Test
    public void find_byValidId_returnsTheMovie() throws Exception {
        // Arrange
        Long movieId = 1L;
        Movie shawshank = MovieMockDataFactory.createShawshankWithId(movieId);
        List<Movie> movies = List.of(shawshank);
        Mockito.when(movieRepository.findAll(any(Specification.class))).thenReturn(movies);
        ObjectMapper mapper = new ObjectMapper();
        String movieJson = mapper.writeValueAsString(movies);

        // Act
        String responseJson = mockMvc.perform(get("/api/movies/find").header("Authorization", "Bearer " + accessToken)
                        .param("id", String.valueOf(movieId)))

                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse()
                .getContentAsString();

        // Assert
        assertThat(responseJson).isEqualTo(movieJson);
    }

    @Test
    public void find_byInvalidId_returnsNotFound() throws Exception {
        // Arrange
        Long movieId = 1L;
        Mockito.when(movieRepository.findById(movieId)).thenReturn(java.util.Optional.empty());

        // Act and Assert
        mockMvc.perform(get("/api/movies/find?id={id}", movieId).header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isNotFound());
    }

    @Test
    public void addMovie_validMovie_returnsCreated() throws Exception {
        // Arrange
        Movie shawshank = MovieMockDataFactory.createShawshankWithoutId();
        Mockito.when(movieRepository.save(shawshank)).thenReturn(shawshank);
        ObjectMapper mapper = new ObjectMapper();
        String movieJson = mapper.writeValueAsString(shawshank);


        // Act and Assert
        mockMvc.perform(post("/api/movies").header("Authorization", "Bearer " + accessToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(movieJson))
                .andExpect(status().isCreated());
    }

}
