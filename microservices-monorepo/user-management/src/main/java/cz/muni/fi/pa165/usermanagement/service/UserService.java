package cz.muni.fi.pa165.usermanagement.service;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The UserService interface provides the blueprint for user management operations.
 * It outlines CRUD methods for user objects.
 */
public interface UserService {

    /**
     * Retrieves all users in the system.
     *
     * @param pageable specification from where we should start
     * @return A page of UserDTO objects.
     */
    Page<User> getAllUsers(Pageable pageable);

    /**
     * Creates a new user in the system.
     *
     * @param user The User entity to be created.
     * @return The created User entity with an assigned ID.
     */
    User createUser(User user);

    /**
     * Updates an existing user in the system.
     * This method assumes that the user parameter contains an ID that identifies the user to be updated.
     *
     * @param user The User entity with updated information.
     * @throws EntityNotFoundException if no user is found with the specified username.
     */
    void updateUser(User user);

    /**
     * Retrieves a user by their ID.
     *
     * @param id The ID of the user to retrieve.
     * @return The User entity if found. Returns null if no user is found with the given ID.
     */
    User getUser(long id);

    /**
     * Retrieves all users of a specified user type as DTOs.
     * This method returns a list of UserDTO objects, each representing a user of the specified user type.
     * If no users match the specified type, or if there are no users at all, an empty list is returned.
     *
     * @param userType The user type to filter users by. This parameter determines which users are returned based
     *                 on their user type.
     * @param pageable specification from where we should start
     * @return A page of UserDTO objects.
     */
    Page<User> getAllUsersWithUserType(UserType userType, Pageable pageable);

    /**
     * Retrieves a user by their email address.
     * This method searches for and returns a user based on the provided email address.
     * The search is case-insensitive and expects an exact match of the email address.
     *
     * @param email The email address of the user to retrieve. The match is case-insensitive.
     * @return The UserDTO object representing the user with the specified email,
     * or null if no user is found with that email address.
     * @throws EntityNotFoundException if no user is found with the specified email address.
     */
    User getUserWithEmail(String email);

    /**
     * Retrieves a user by their username.
     * This method looks up a user based on the username provided. The search is case-sensitive,
     * and the username must exactly match the stored value.
     *
     * @param username The username of the user to retrieve. The match is case-sensitive.
     * @return The UserDTO object representing the user with the specified username,
     * or null if no user is found with that username.
     * @throws EntityNotFoundException if no user is found with the specified username.
     */
    User getUserWithUsername(String username);


    /**
     * Deletes a user by their ID.
     *
     * @param id The ID of the user to delete.
     * @throws EntityNotFoundException if no user is found with the specified username.
     */
    void deleteUser(long id);
}
