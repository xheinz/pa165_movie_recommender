package cz.muni.fi.pa165.usermanagement.config;

import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
@Profile("clear-db")
public class ClearDatabaseService {

    private final UserRepository userRepository;

    @PostConstruct
    public void clearData() {
        userRepository.deleteAll();
    }
}
