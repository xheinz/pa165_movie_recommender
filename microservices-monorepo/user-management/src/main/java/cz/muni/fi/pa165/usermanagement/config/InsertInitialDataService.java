package cz.muni.fi.pa165.usermanagement.config;

import com.github.javafaker.Faker;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
@Profile("db-init")
public class InsertInitialDataService {

    public static final int GENERATED_DATA_AMOUNT = 50;
    private final UserRepository userRepository;
    private final Faker faker = new Faker();


    @PostConstruct
    public void init() {
        if (userRepository.count() == 0) {
            seedData();
        }
    }

    public void seedData() {
        List<User> users = new ArrayList<>();
        for (int i = 0; i < GENERATED_DATA_AMOUNT; i++) {
            User user = new User();
            user.setUserName(faker.name().username() + i);
            user.setEmail(i + faker.internet().emailAddress());
            user.setEncryptedPassword(faker.internet().password());
            user.setUserType(UserType.values()[faker.random().nextInt(UserType.values().length)]);
            users.add(user);
        }
        userRepository.saveAll(users);
    }
}
