package cz.muni.fi.pa165.usermanagement.facade;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * The UserFacade interface provides a high-level abstraction for user management operations.
 * It offers methods to handle user data transfer objects (DTOs) for creating, updating,
 * retrieving, and deleting users.
 */
public interface UserFacade {

    /**
     * Retrieves all users as DTOs.
     *
     * @param pageable specification from where we should start
     * @return A page of UserDTO objects.
     */
    Page<UserDTO> getAllUsers(Pageable pageable);

    /**
     * Retrieves all users of a specified user type as DTOs.
     * This method returns a list of UserDTO objects, each representing a user of the specified user type.
     * If no users match the specified type, or if there are no users at all, an empty list is returned.
     *
     * @param userType The user type to filter users by. This parameter determines which users are returned
     *                 based on their user type.
     * @param pageable specification from where we should start
     * @return A page of UserDTO objects.
     */
    Page<UserDTO> getAllUsersWithUserType(UserType userType, Pageable pageable);

    /**
     * Retrieves a user by their email address.
     * This method searches for and returns a user based on the provided email address.
     * The search is case-insensitive and expects an exact match of the email address.
     *
     * @param email The email address of the user to retrieve. The match is case-insensitive.
     * @return The {@link UserDTO} object representing the user with the specified email,
     * or null if no user is found with that email address.
     * @throws EntityNotFoundException if no user is found with the specified email address.
     */
    UserDTO getUserWithEmail(String email);

    /**
     * Retrieves a user by their username.
     * This method looks up a user based on the username provided. The search is case-sensitive,
     * and the username must exactly match the stored value.
     *
     * @param username The username of the user to retrieve. The match is case-sensitive.
     * @return The {@link UserDTO} object representing the user with the specified username,
     * or null if no user is found with that username.
     * @throws EntityNotFoundException if no user is found with the specified username.
     */
    UserDTO getUserWithUsername(String username);

    /**
     * Creates a new user based on the provided UserRegistrationDTO.
     *
     * @param userDTO The UserRegistrationDTO containing the data necessary to create a new user.
     * @return The created UserDTO, representing the newly created user with its assigned ID and relevant data.
     */
    UserDTO createUser(UserRegistrationDTO userDTO);

    /**
     * Updates an existing user identified by the given ID with the data provided in the UserUpdateDTO.
     *
     * @param id      The ID of the user to be updated.
     * @param userDTO The UserUpdateDTO containing the updated user data.
     * @throws EntityNotFoundException if no user is found.
     */
    void updateUser(long id, UserUpdateDTO userDTO);

    /**
     * Retrieves a single user by their ID, returning the data as a UserDTO.
     *
     * @param id The ID of the user to retrieve.
     * @return The UserDTO representing the retrieved user.
     * @throws EntityNotFoundException if no user is found with the specified username.
     */
    UserDTO getUser(long id);

    /**
     * Deletes a user identified by the given ID.
     *
     * @param id The ID of the user to delete.
     * @throws EntityNotFoundException if no user is found.
     */
    void deleteUser(long id);
}
