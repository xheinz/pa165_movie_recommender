package cz.muni.fi.pa165.usermanagement.data.enums;

/**
 * Represents the types of users in the system, distinguishing between administrative and regular users.
 * Each user type defines whether it possesses administrative rights through
 * the {@link #hasAdministrativeRights()} method.
 */
public enum UserType {

    /**
     * The ADMIN user type represents users with administrative privileges.
     * Users of this type have permissions to perform administrative actions within the system.
     */
    ADMIN {
        @Override
        public boolean hasAdministrativeRights() {
            return true;
        }
    },

    /**
     * The USER type represents standard users without administrative privileges.
     * Users of this type are limited to actions that do not require administrative rights.
     */
    USER {
        @Override
        public boolean hasAdministrativeRights() {
            return false;
        }
    };

    /**
     * Determines if the user type has administrative rights.
     *
     * @return {@code true} if the user type has administrative rights, {@code false} otherwise.
     */
    protected abstract boolean hasAdministrativeRights();
}
