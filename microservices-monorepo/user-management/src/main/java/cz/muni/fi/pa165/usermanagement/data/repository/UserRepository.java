package cz.muni.fi.pa165.usermanagement.data.repository;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Interface for repository handling persistence operations on User entities.
 * Extends {@link JpaRepository} to provide CRUD operations and additional JPA-specific functionality.
 * This repository interface facilitates the management of User entity data with methods for querying,
 * counting, and retrieving users based on different attributes and conditions.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Retrieves a user by their username.
     *
     * @param userName the username to search for.
     * @return the {@link User} with the specified username, or null if no such user exists.
     */
    Optional<User> findByUserName(String userName);

    /**
     * Retrieves a user by their exact email.
     *
     * @param email the exact email to search for.
     * @return the {@link User} with the specified email, or null if no such user exists.
     */
    Optional<User> findByEmail(String email);

    /**
     * Retrieves all users of a specific user type.
     *
     * @param userType the {@link UserType} to filter the users by.
     * @param pageable specification from where we should start
     * @return a list of {@link User} entities that match the specified user type.
     * The list can be empty if no users match.
     */
    Page<User> findAllByUserTypeIs(UserType userType, Pageable pageable);

    /**
     * Counts all users of a specific user type.
     *
     * @param userType the {@link UserType} to count the users for.
     * @return the number of users that have the specified user type.
     */
    Integer countAllByUserTypeIs(UserType userType);
}
