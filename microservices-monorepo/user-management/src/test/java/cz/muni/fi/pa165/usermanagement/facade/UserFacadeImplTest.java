package cz.muni.fi.pa165.usermanagement.facade;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.service.UserService;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import cz.muni.fi.pa165.usermanagement.util.mappers.UserMapper;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;


@ExtendWith(MockitoExtension.class)
class UserFacadeImplTest {

    public static final long FIRST_USER_ID = 1L;

    @Mock
    private UserService userService;

    @Mock
    private UserMapper userMapper;

    @InjectMocks
    private UserFacadeImpl userFacade;

    @Test
    void getAllUsers() {
        Pageable pageable = PageRequest.of(0, 10);
        List<User> users = TestDataFactory.createUserMultiple(5L);
        List<UserDTO> userDTOs = TestDataFactory.createUserDTOMultiple(5L);
        Page<User> page = new PageImpl<>(users);

        Mockito.when(userService.getAllUsers(pageable)).thenReturn(page);
        Mockito.when(userMapper.userToUserDTO(Mockito.any(User.class))).thenAnswer(invocation -> {
            User user = invocation.getArgument(0);
            return userDTOs.stream().filter(dto -> dto.id() == user.getId()).findFirst().orElse(null);
        });

        Page<UserDTO> result = userFacade.getAllUsers(pageable);

        Assertions.assertNotNull(result);
        Assertions.assertFalse(result.isEmpty());
        Assertions.assertEquals(userDTOs.size(), result.getContent().size());

        Mockito.verify(userService, Mockito.times(1)).getAllUsers(pageable);
        Mockito.verify(userMapper, Mockito.times(users.size())).userToUserDTO(Mockito.any(User.class));
    }

    /**
     * Test method for {@link UserFacadeImpl#createUser(UserRegistrationDTO)}.
     */
    @ParameterizedTest
    @EnumSource(UserType.class)
    void createUser(final UserType userType) {
        User user = TestDataFactory.createUser(FIRST_USER_ID, userType);
        UserDTO userDTO = TestDataFactory.createUserDTO(FIRST_USER_ID, userType);
        UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO(
            "newUser", userType
        );

        Mockito.when(userMapper.userRegistrationDTOToUser(userRegistrationDTO)).thenReturn(user);
        Mockito.when(userService.createUser(user)).thenReturn(user);
        Mockito.when(userMapper.userToUserDTO(user)).thenReturn(userDTO);

        UserDTO result = userFacade.createUser(userRegistrationDTO);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(userDTO.id(), result.id());
        Assertions.assertEquals(userDTO.userName(), result.userName());
        Assertions.assertEquals(userDTO.email(), result.email());
        Assertions.assertEquals(userDTO.userType(), userType);

        Mockito.verify(userService, Mockito.times(1)).createUser(user);
    }

    /**
     * Test method for {@link UserFacadeImpl#updateUser(long, UserUpdateDTO)}.
     */
    @ParameterizedTest
    @EnumSource(UserType.class)
    void updateUser_success(final UserType userType) {
        User user = TestDataFactory.createUser(FIRST_USER_ID, userType);
        UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("newUser", userType);

        Mockito.when(userService.getUser(FIRST_USER_ID)).thenReturn(user);

        userFacade.updateUser(FIRST_USER_ID, userUpdateDTO);

        Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
        Mockito.verify(userService, Mockito.times(1)).updateUser(user);
    }

    /**
     * Test method for {@link UserFacadeImpl#updateUser(long, UserUpdateDTO)}.
     */
    @Test
    void updateUser_notFound() {
        UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("newUser", UserType.USER);
        Mockito.when(userService.getUser(FIRST_USER_ID)).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(
            EntityNotFoundException.class, () -> userFacade.updateUser(FIRST_USER_ID, userUpdateDTO)
        );

        Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
    }

    /**
     * Test method for {@link UserFacadeImpl#getUser(long)}.
     */
    @ParameterizedTest
    @EnumSource(UserType.class)
    void getUser_success(final UserType userType) {
        User user = TestDataFactory.createUser(FIRST_USER_ID, userType);
        UserDTO userDTO = TestDataFactory.createUserDTO(FIRST_USER_ID, userType);

        Mockito.when(userService.getUser(FIRST_USER_ID)).thenReturn(user);
        Mockito.when(userMapper.userToUserDTO(user)).thenReturn(userDTO);

        UserDTO result = userFacade.getUser(FIRST_USER_ID);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(userDTO.id(), result.id());
        Assertions.assertEquals(userDTO.userName(), result.userName());
        Assertions.assertEquals(userDTO.email(), result.email());
        Assertions.assertEquals(userDTO.userType(), userType);

        Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
    }

    /**
     * Test method for {@link UserFacadeImpl#getUser(long)}.
     */
    @Test
    void getUser_notFound() {
        Mockito.when(userService.getUser(FIRST_USER_ID)).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class, () -> userFacade.getUser(FIRST_USER_ID));

        Mockito.verify(userService, Mockito.times(1)).getUser(FIRST_USER_ID);
    }

    /**
     * Test method for {@link UserFacadeImpl#deleteUser(long)}.
     */
    @Test
    void deleteUser() {
        userFacade.deleteUser(FIRST_USER_ID);    // deleteUser doesn't return anything yet, can't test success/failure

        Mockito.verify(userService, Mockito.times(1)).deleteUser(FIRST_USER_ID);
    }

    @Test
    void getAllUsersWithUserType_ValidUserType_ReturnsFilteredUsers() {
        Pageable pageable = PageRequest.of(0, 10);
        List<User> users = TestDataFactory.createUserMultiple(5);
        List<UserDTO> userDTOs = TestDataFactory.createUserDTOMultiple(5);
        Page<User> page = new PageImpl<>(users);

        Mockito.when(userService.getAllUsersWithUserType(UserType.ADMIN, pageable)).thenReturn(page);
        Mockito.when(userMapper.userToUserDTO(Mockito.any(User.class))).thenAnswer(invocation -> {
            User user = invocation.getArgument(0);
            return userDTOs.stream().filter(dto -> dto.id() == user.getId()).findFirst().orElse(null);
        });

        Page<UserDTO> result = userFacade.getAllUsersWithUserType(UserType.ADMIN, pageable);

        Assertions.assertNotNull(result);
        Assertions.assertFalse(result.isEmpty());
        Assertions.assertEquals(userDTOs.size(), result.getContent().size());

        Mockito.verify(userService, Mockito.times(1))
            .getAllUsersWithUserType(UserType.ADMIN, pageable);
        Mockito.verify(userMapper, Mockito.times(users.size())).userToUserDTO(Mockito.any(User.class));
    }

    @Test
    void getAllUsersWithUserType_InvalidUserType_ReturnsEmptyList() {
        Pageable pageable = PageRequest.of(0, 10);
        Mockito.when(userService.getAllUsersWithUserType(UserType.USER, pageable)).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class, () ->
            userFacade.getAllUsersWithUserType(UserType.USER, pageable));

        Mockito.verify(userService, Mockito.times(1)).getAllUsersWithUserType(UserType.USER, pageable);
    }

    @Test
    void getUserWithEmail_ValidEmail_ReturnsUser() {
        User user = TestDataFactory.createUserWithEmail("user@example.com");
        UserDTO userDTO = TestDataFactory.createUserDTOWithEmail("user@example.com");

        Mockito.when(userService.getUserWithEmail("user@example.com")).thenReturn(user);
        Mockito.when(userMapper.userToUserDTO(user)).thenReturn(userDTO);

        UserDTO result = userFacade.getUserWithEmail("user@example.com");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(userDTO.email(), result.email());
        Mockito.verify(userService, Mockito.times(1)).getUserWithEmail("user@example.com");
    }

    @Test
    void getUserWithEmail_InvalidEmail_ThrowsEntityNotFoundException() {
        Mockito.when(userService.getUserWithEmail("notfound@example.com")).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class, () -> userFacade.getUserWithEmail("notfound@example.com"));

        Mockito.verify(userService, Mockito.times(1)).getUserWithEmail("notfound@example.com");
    }

    @Test
    void getUserWithUsername_ValidUsername_ReturnsUser() {
        User user = TestDataFactory.createUserWithUsername("validUsername");
        UserDTO userDTO = TestDataFactory.createUserDTOWithUsername("validUsername");

        Mockito.when(userService.getUserWithUsername("validUsername")).thenReturn(user);
        Mockito.when(userMapper.userToUserDTO(user)).thenReturn(userDTO);

        UserDTO result = userFacade.getUserWithUsername("validUsername");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(userDTO.userName(), result.userName());
        Mockito.verify(userService, Mockito.times(1)).getUserWithUsername("validUsername");
    }

    @Test
    void getUserWithUsername_InvalidUsername_ThrowsEntityNotFoundException() {
        Mockito.when(userService.getUserWithUsername("invalidUsername")).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class, () -> userFacade.getUserWithUsername("invalidUsername"));

        Mockito.verify(userService, Mockito.times(1)).getUserWithUsername("invalidUsername");
    }
}
