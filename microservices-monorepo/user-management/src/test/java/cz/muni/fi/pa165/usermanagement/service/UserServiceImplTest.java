package cz.muni.fi.pa165.usermanagement.service;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    public static final long FIRST_USER_ID = 1L;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    void getAllUsers() {
        Pageable pageable = PageRequest.of(0, 5);
        List<User> users = TestDataFactory.createUserMultiple(5L);
        Page<User> page = new PageImpl<>(users);

        Mockito.when(userRepository.findAll(pageable)).thenReturn(page);

        Page<User> result = userService.getAllUsers(pageable);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, page);
        Mockito.verify(userRepository, Mockito.times(1)).findAll(pageable);
    }

    /**
     * Test method for {@link UserServiceImpl#createUser(User)}.
     */
    @ParameterizedTest
    @EnumSource(UserType.class)
    void createUser(final UserType userType) {
        User user = TestDataFactory.createUser(FIRST_USER_ID, userType);

        Mockito.when(userRepository.save(user)).thenReturn(user);

        User result = userService.createUser(user);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(user.getId(), result.getId());
        Assertions.assertEquals(user.getUserName(), result.getUserName());
        Assertions.assertEquals(user.getEmail(), result.getEmail());
        Assertions.assertEquals(user.getUserType(), userType);
        Mockito.verify(userRepository, Mockito.times(1)).save(user);
    }

    /**
     * Test method for {@link UserServiceImpl#getUser(long)}.
     */
    @ParameterizedTest
    @EnumSource(UserType.class)
    void getUser_success(final UserType userType) {
        User user = TestDataFactory.createUser(FIRST_USER_ID, userType);

        Mockito.when(userRepository.findById(FIRST_USER_ID)).thenReturn(Optional.of(user));

        User result = userService.getUser(FIRST_USER_ID);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(user.getId(), result.getId());
        Assertions.assertEquals(user.getUserName(), result.getUserName());
        Assertions.assertEquals(user.getEmail(), result.getEmail());
        Assertions.assertEquals(user.getUserType(), userType);
        Mockito.verify(userRepository, Mockito.times(1)).findById(FIRST_USER_ID);
    }

    /**
     * Test method for {@link UserServiceImpl#getUser(long)}.
     */
    @Test
    void getUser_notFound() {
        Mockito.when(userRepository.findById(FIRST_USER_ID)).thenThrow(EntityNotFoundException.class);

        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getUser(FIRST_USER_ID));
        Mockito.verify(userRepository, Mockito.times(1)).findById(FIRST_USER_ID);
    }

    /**
     * Test method for {@link UserServiceImpl#updateUser(User)}.
     */
    @Test
    void updateUser() {
        User user = TestDataFactory.createUser(FIRST_USER_ID, UserType.USER);

        Mockito.when(userRepository.save(user)).thenReturn(user);

        userService.updateUser(user);

        Mockito.verify(userRepository, Mockito.times(1)).save(user);
    }

    /**
     * Test method for {@link UserServiceImpl#deleteUser(long)}.
     */
    @Test
    void deleteUser() {
        userService.deleteUser(FIRST_USER_ID);    // deleteUser doesn't return anything yet, can't test success/failure

        Mockito.verify(userRepository, Mockito.times(1)).deleteById(FIRST_USER_ID);
    }

    @Test
    void getAllUsersWithUserType_ValidUserType_ReturnsUsers() {
        Pageable pageable = PageRequest.of(0, 5);
        List<User> users = TestDataFactory.createUserMultiple(5L, UserType.ADMIN);
        Page<User> page = new PageImpl<>(users);

        Mockito.when(userRepository.findAllByUserTypeIs(UserType.ADMIN, pageable)).thenReturn(page);

        Page<User> result = userService.getAllUsersWithUserType(UserType.ADMIN, pageable);

        Assertions.assertNotNull(result);
        Assertions.assertEquals(result, page);
        Mockito.verify(userRepository, Mockito.times(1))
            .findAllByUserTypeIs(UserType.ADMIN, pageable);
    }

    @Test
    void getAllUsersWithUserType_Empty_ReturnsEmptyList() {
        Pageable pageable = PageRequest.of(0, 5);
        Page<User> page = new PageImpl<>(new ArrayList<>());

        Mockito.when(userRepository.findAllByUserTypeIs(UserType.USER, pageable)).thenReturn(page);

        Page<User> result = userService.getAllUsersWithUserType(UserType.USER, pageable);

        Assertions.assertNotNull(result);
        Assertions.assertTrue(result.isEmpty());
        Mockito.verify(userRepository, Mockito.times(1))
            .findAllByUserTypeIs(UserType.USER, pageable);
    }

    @Test
    void getUserWithEmail_ValidEmail_ReturnsUser() {
        User user = TestDataFactory.createUserWithEmail("user@example.com");

        Mockito.when(userRepository.findByEmail("user@example.com")).thenReturn(Optional.of(user));

        User result = userService.getUserWithEmail("user@example.com");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(user.getEmail(), result.getEmail());
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail("user@example.com");
    }

    @Test
    void getUserWithEmail_InvalidEmail_ThrowsEntityNotFoundException() {
        Mockito.when(userRepository.findByEmail("notfound@example.com")).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getUserWithEmail("notfound@example.com"));
        Mockito.verify(userRepository, Mockito.times(1)).findByEmail("notfound@example.com");
    }

    @Test
    void getUserWithUsername_ValidUsername_ReturnsUser() {
        User user = TestDataFactory.createUserWithUsername("validUsername");

        Mockito.when(userRepository.findByUserName("validUsername")).thenReturn(Optional.of(user));

        User result = userService.getUserWithUsername("validUsername");

        Assertions.assertNotNull(result);
        Assertions.assertEquals(user.getUserName(), result.getUserName());
        Mockito.verify(userRepository, Mockito.times(1)).findByUserName("validUsername");
    }

    @Test
    void getUserWithUsername_InvalidUsername_ThrowsEntityNotFoundException() {
        Mockito.when(userRepository.findByUserName("invalidUsername")).thenReturn(Optional.empty());

        Assertions.assertThrows(EntityNotFoundException.class, () -> userService.getUserWithUsername("invalidUsername"));
        Mockito.verify(userRepository, Mockito.times(1)).findByUserName("invalidUsername");
    }
}
