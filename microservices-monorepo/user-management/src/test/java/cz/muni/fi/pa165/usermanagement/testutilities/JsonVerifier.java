package cz.muni.fi.pa165.usermanagement.testutilities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import org.junit.jupiter.api.Assertions;

public final class JsonVerifier {

    private JsonVerifier() {
    }

    public static void verifyJsonResponse(String jsonResponse, UserType userType) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(jsonResponse);

        Assertions.assertEquals(0, rootNode.path("number").asInt());
        Assertions.assertEquals(10, rootNode.path("size").asInt());
        Assertions.assertEquals(10, rootNode.path("totalElements").asInt());
        Assertions.assertEquals(1, rootNode.path("totalPages").asInt());
        Assertions.assertTrue(rootNode.path("first").asBoolean());
        Assertions.assertTrue(rootNode.path("last").asBoolean());

        JsonNode content = rootNode.path("content");
        Assertions.assertFalse(content.isEmpty());
        for (JsonNode userNode : content) {
            Assertions.assertEquals(userType.toString(), userNode.path("userType").asText());
            Assertions.assertTrue(userNode.path("id").asInt() > 0);
            Assertions.assertNotNull(userNode.path("userName").asText());
            Assertions.assertNotNull(userNode.path("email").asText());
        }
    }
}
