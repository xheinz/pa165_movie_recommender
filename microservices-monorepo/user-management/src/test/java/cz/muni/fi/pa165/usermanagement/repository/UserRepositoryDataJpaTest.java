package cz.muni.fi.pa165.usermanagement.repository;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class UserRepositoryDataJpaTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByUserName_WhenExists_ReturnsUser() {
        User user = TestDataFactory.createUserToStoreWithUsername("testUser");

        entityManager.persist(user);
        entityManager.flush();

        Optional<User> foundUser = userRepository.findByUserName("testUser");
        assertThat(foundUser).isPresent().hasValueSatisfying(u -> assertThat(u.getUserName()).isEqualTo("testUser"));
    }

    @Test
    public void findByUserName_DoesNotExists_ReturnsEmppty() {
        Optional<User> foundUser = userRepository.findByUserName("testUser");
        assertThat(foundUser).isEmpty();
    }

    @Test
    public void findByEmail_WhenExists_ReturnsUser() {
        User user = TestDataFactory.createUserToStoreWithEmail("unique@example.com");

        entityManager.persist(user);
        entityManager.flush();

        Optional<User> foundUser = userRepository.findByEmail("unique@example.com");
        assertThat(foundUser).isPresent().hasValueSatisfying(u -> assertThat(u.getEmail()).isEqualTo("unique@example.com"));
    }

    @Test
    public void findByEmail_DoesNotExists_ReturnsEmppty() {
        Optional<User> foundUser = userRepository.findByEmail("unique@example.com");
        assertThat(foundUser).isEmpty();
    }

    @Test
    public void findAllByUserTypeIs_WhenExists_ReturnsUsers() {
        Pageable pageable = PageRequest.of(0, 2);
        User user1 = TestDataFactory.createUserToStore(UserType.ADMIN);
        User user2 = TestDataFactory.createUserToStore(UserType.ADMIN);

        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.flush();

        Page<User> users = userRepository.findAllByUserTypeIs(UserType.ADMIN, pageable);
        assertThat(users).hasSize(2).allMatch(user -> user.getUserType() == UserType.ADMIN);
    }

    @Test
    public void findAllByUserTypeIs_WhenExists_ReturnsCorrectUsersTypes() {
        Pageable pageable = PageRequest.of(0, 3);
        Pageable pageable2 = PageRequest.of(0, 5);
        List<User> adminUsers = TestDataFactory.createUsersToStore(3, UserType.ADMIN);
        List<User> userUsers = TestDataFactory.createUsersToStore(5, UserType.USER);

        adminUsers.forEach(user -> entityManager.persist(user));
        userUsers.forEach(user -> entityManager.persist(user));
        entityManager.flush();

        Page<User> resultAdmins = userRepository.findAllByUserTypeIs(UserType.ADMIN, pageable);
        assertThat(resultAdmins).hasSize(3).allMatch(user -> user.getUserType() == UserType.ADMIN);

        Page<User> resultUsers = userRepository.findAllByUserTypeIs(UserType.USER, pageable2);
        assertThat(resultUsers).hasSize(5).allMatch(user -> user.getUserType() == UserType.USER);
    }

    @Test
    public void countAllByUserTypeIs_WhenExists_ReturnsCorrectUserTypeCount() {
        List<User> adminUsers = TestDataFactory.createUsersToStore(7, UserType.ADMIN);
        List<User> userUsers = TestDataFactory.createUsersToStore(5, UserType.USER);

        adminUsers.forEach(user -> entityManager.persist(user));
        userUsers.forEach(user -> entityManager.persist(user));
        entityManager.flush();

        Integer countAdmins = userRepository.countAllByUserTypeIs(UserType.ADMIN);
        assertThat(countAdmins).isEqualTo(7);

        Integer countUsers = userRepository.countAllByUserTypeIs(UserType.USER);
        assertThat(countUsers).isEqualTo(5);
    }

    @Test
    public void countAllByUserTypeIs_WhenExists_ReturnsCount() {
        User user1 = TestDataFactory.createUserToStore(UserType.USER);
        User user2 = TestDataFactory.createUserToStore(UserType.USER);

        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.flush();

        Integer count = userRepository.countAllByUserTypeIs(UserType.USER);
        assertThat(count).isEqualTo(2);
    }
}
