package cz.muni.fi.pa165.ratingsandreviews.review;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.repository.ReviewRepository;
import cz.muni.fi.pa165.ratingsandreviews.utils.ReviewMockDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class ReviewRepositoryTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void initData() {
        List<Review> reviews = ReviewMockDataFactory.mockAllReviews();
        reviews.forEach(entityManager::persist);
    }

    @Test
    public void whenFindByUserID_thenReturnReview() {
        List<Review> found = reviewRepository.findByUserId(1L);

        assertEquals(1L, (long) found.getFirst().getUserId());
        assertEquals(5, found.size());
    }

    @Test
    void findByUserId_notFoundId_returnsEmpty() {
        Optional<Review> found = reviewRepository.findById(999L);
        assertTrue(found.isEmpty());
    }

}
