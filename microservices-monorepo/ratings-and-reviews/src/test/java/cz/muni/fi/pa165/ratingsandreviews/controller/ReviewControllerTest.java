package cz.muni.fi.pa165.ratingsandreviews.controller;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.facade.ReviewFacade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class ReviewControllerTest {

    @Mock
    private ReviewFacade reviewFacade;

    @InjectMocks
    private ReviewController reviewController;

    @Test
    void addReview_validReview_reviewAdded() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        ReviewDTO addedReview = new ReviewDTO();
        Mockito.when(reviewFacade.addReview(review)).thenReturn(addedReview);

        // Act
        ReviewDTO result = reviewController.addReview(review).getBody();

        // Assert
        Assertions.assertEquals(addedReview, result);
    }

    @Test
    void addReview_invalidReview_reviewNotAdded() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.addReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.addReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void deleteReview_validId_reviewDeleted() {
        // Arrange
        Mockito.when(reviewFacade.deleteReview(1L)).thenReturn(true);

        // Act
        Long result = reviewController.deleteReview(1L).getBody();

        // Assert
        Assertions.assertEquals(1L, result);
    }

    @Test
    void deleteReview_invalidId_reviewNotDeleted() {
        // Arrange
        Mockito.when(reviewFacade.deleteReview(1L)).thenReturn(false);

        // Act
        Long result = reviewController.deleteReview(1L).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void updateReview_validUpdateForExistingMovie_reviewUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        ReviewDTO updatedReview = new ReviewDTO();
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(updatedReview);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertEquals(updatedReview, result);
    }

    @Test
    void updateReview_invalidUpdateForExistingMovie_reviewNotUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void updateReview_validUpdateForNonExistingMovie_reviewNotUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        review.setId(1L);
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void updateReview_invalidUpdateForNonExistingMovie_reviewNotUpdated() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.updateReview(review)).thenReturn(null);

        // Act
        ReviewDTO result = reviewController.updateReview(review).getBody();

        // Assert
        Assertions.assertNull(result);
    }

    @Test
    void getReview_validId_returnsReview() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.getReview(1L)).thenReturn(Optional.of(review));

        // Act
        ReviewDTO result = reviewController.getReview(1L).getBody();

        // Assert
        Assertions.assertEquals(review, result);
    }

    @Test
    void getReview_invalidId_returnsNull() {
        // Arrange
        Mockito.when(reviewFacade.getReview(1L)).thenReturn(Optional.empty());

        // Act
        ReviewDTO result = reviewController.getReview(1L).getBody();

        // Assert
        Assertions.assertNull(result);
    }


    @Test
    void getReviewsByUserId_existingUser_returnsReviews() {
        // Arrange
        ReviewDTO review = new ReviewDTO();
        Mockito.when(reviewFacade.getReviewsByUserId(1L)).thenReturn(java.util.Collections.singletonList(review));

        // Act
        List<ReviewDTO> resultBody = Objects.requireNonNull(reviewController.getReviewsByUserId(1L).getBody()).getContent();

        // Assert
        Assertions.assertNotNull(resultBody, "The result body should not be null");
        Assertions.assertFalse(resultBody.isEmpty(), "The result body should not be empty");
        ReviewDTO result = resultBody.getFirst();
        Assertions.assertEquals(review, result, "The returned review should match the expected review");
    }

    @Test
    void getReviewsByUserId_nonExistingUser_returnsEmptyList() {
        // Arrange
        Mockito.when(reviewFacade.getReviewsByUserId(1L)).thenReturn(java.util.Collections.emptyList());

        // Act
        ResponseEntity<Page<ReviewDTO>> result = reviewController.getReviewsByUserId(1L);

        // Assert

        Assertions.assertEquals(result.getStatusCode(), HttpStatus.OK);
        Assertions.assertEquals(result.getBody().getContent(), java.util.Collections.emptyList());
    }
}
