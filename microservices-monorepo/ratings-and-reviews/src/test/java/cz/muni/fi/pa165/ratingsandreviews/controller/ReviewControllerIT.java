package cz.muni.fi.pa165.ratingsandreviews.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.repository.ReviewRepository;
import cz.muni.fi.pa165.ratingsandreviews.utils.KeycloakTokenProvider;
import cz.muni.fi.pa165.ratingsandreviews.utils.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class ReviewControllerIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private KeycloakTokenProvider keycloakTokenProvider;

    @MockBean
    private ReviewRepository reviewRepository;

    private String accessToken;
    @BeforeEach
    public void setup() {
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }

    @Test
    void getReviewById_whenFound_returnsReview() throws Exception {
        // Arrange
        long reviewId = 1L;
        Review expectedReview = new Review();
        expectedReview.setId(reviewId);
        expectedReview.setMovieId(2L);
        expectedReview.setRatings(null);
        expectedReview.setUserId(3L);
        expectedReview.setOverallRating(4.5);

        when(reviewRepository.findById(any())).thenReturn(Optional.of(expectedReview));

        // Act & Assert
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews/{reviewId}", reviewId)
                        .header("Authorization", "Bearer " + accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        assertEquals("{\"movieId\":2,\"userId\":3,\"id\":1,\"ratings\":null,\"overallRating\":4.5}", responseJson);
    }

    @Test
    void getReviews_found_returnsReviews() throws Exception {
        // Arrange
        List<Review> expectedReview = TestDataFactory.createReviewList(1);

        when(reviewRepository.findAll((Specification<Review>) any())).thenReturn(expectedReview);

        // Act & Assert
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/reviews")
                        .header("Authorization", "Bearer " + accessToken)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        JsonNode expectedJson = new ObjectMapper().readTree(responseJson);
        assertEquals("[{\"movieId\":2,\"userId\":2,\"id\":1,\"ratings\":null,\"overallRating\":1.0}]", expectedJson.get("content").toString());
        assertEquals("1", expectedJson.get("totalElements").toString());
    }

    @Test
    void addReview_validReview_returnsCreatedAndReviewDTO() throws Exception {
        // Arrange
        Review newReview = TestDataFactory.createReview(1L);

        when(reviewRepository.save(newReview)).thenReturn(newReview);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.post("/api/reviews")
                        .header("Authorization", "Bearer " + accessToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(newReview)))
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);

        // Assert
        JsonNode expectedJson = new ObjectMapper().readTree(responseJson);
        assertEquals("{\"movieId\":2,\"userId\":2,\"id\":1,\"ratings\":null,\"overallRating\":1.0}", expectedJson.toString());
    }


    private static String asJsonString(final Object obj) throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(obj);
    }

}
