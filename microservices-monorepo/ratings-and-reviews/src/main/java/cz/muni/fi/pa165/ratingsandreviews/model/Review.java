package cz.muni.fi.pa165.ratingsandreviews.model;

import java.util.Map;
import java.util.HashMap;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.MapKeyColumn;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Entity representing a review for a movie.
 */
@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "reviews")
public class Review {
    /**
     * The unique identifier for the review.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The ID of the movie being reviewed.
     */
    @Column(name = "movie_id")
    private Long movieId;

    /**
     * The ID of the user who created the review.
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * The overall rating given to the movie in the review.
     */
    @Column(name = "overall_rating")
    private double overallRating;

    /**
     * The ratings for different properties of the movie.
     */
    @ElementCollection
    @CollectionTable(name = "review_ratings", joinColumns = @JoinColumn(name = "review_id"))
    @MapKeyColumn(name = "property")
    @Column(name = "rating")
    private Map<FilmProperty, Float> ratings = new HashMap<>();
}
