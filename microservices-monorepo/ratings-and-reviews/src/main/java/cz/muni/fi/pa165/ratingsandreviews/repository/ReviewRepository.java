package cz.muni.fi.pa165.ratingsandreviews.repository;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<Review, Long>, JpaSpecificationExecutor<Review> {

    /**
     * Finds a list of reviews written by a specific user identified by the given user ID.
     *
     * @param userId the ID of the user whose reviews to retrieve
     * @return a list of Review objects for the given user, or an empty list if no reviews are found
     */
    @Query("SELECT r FROM Review r WHERE r.userId = :userId")
    List<Review> findByUserId(@Param("userId") Long userId);

    /**
     * Finds a list of reviews for a specific movie identified by the given movie ID.
     *
     * @param movieId the ID of the movie whose reviews to retrieve
     * @return a list of Review objects for the given movie, or an empty list if no reviews are found
     */
    @Query("SELECT r FROM Review r WHERE r.movieId = :movieId")
    List<Review> findByMovieId(@Param("movieId") Long movieId);
}


