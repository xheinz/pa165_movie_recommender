package cz.muni.fi.pa165.ratingsandreviews.config;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.repository.ReviewRepository;
import cz.muni.fi.pa165.ratingsandreviews.utils.ReviewMockDataFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
@Configuration
public class DatabaseInitializer {
    /**
     * @param repository the ReviewRepository instance used to interact with the database
     * @return a CommandLineRunner bean that populates the database with mock review data
     */
    @Bean
    public CommandLineRunner initDatabase(ReviewRepository repository) {
        return args -> {
            List<Review> reviewList = ReviewMockDataFactory.mockAllReviews();
            repository.saveAll(reviewList);
        };
    }

}
