import math
import random

from keycloak import KeycloakOpenID
from locust import HttpUser, task, between


class MovieUserBehavior(HttpUser):
    wait_time = between(1, 3)

    def on_start(self):
        print("Starting user behavior")
        """ Login via Keycloak and setup host addresses """

        keycloak_openid = KeycloakOpenID(server_url="http://localhost:8080",
                                         client_id="movie-recommender",
                                         realm_name="MoviesRecommender",
                                         client_secret_key="pMG3OYcz9zGOKtC9l75ubXmcTz34XpHC")
        token = keycloak_openid.token("admin", "admin")

        self.headers = {
            "Authorization": f"Bearer {token}"
        }

        self.keycloak_host = "http://localhost:8080"
        self.movies_host = "http://localhost:8081"
        self.reviews_host = "http://localhost:8082"
        self.recommendations_host = "http://localhost:8083"
        self.users_host = "http://localhost:8084"

    """
        ## Scenario 1: experienced user usage
        1. User logs in
    
        2. User rates a recently watched movie.
        3. User searches for movies by genre, looking for something similar to the last movie watched
        4. Not finding anything appealing, the user turns to the recommendation system.
        5. Still not finding anything of interest, the user reviews his list of top-rated movies.
        6. User updates the rating for "Bastardi," marking it lower due to a change in opinion since first viewing it at a younger age
        7. The user's list of favorite movies is updated, and "Bastardi" is no longer included
        8. User logs out.
    """
    @task
    def experienced_user_scenario(self):
        print("Experienced user scenario")
        user_id = 1
        review_id = 1
        movie_id = 1
        # login
        self.client.get(f"{self.users_host}/api/users/{user_id}", headers=self.headers)
        # rate a recently watched movie
        review = {
            "movieId": movie_id,
            "userId": user_id,
            "id": math.floor(random.random() * 1000),
            "overallRating": 4.1,
        }
        self.client.post(f"{self.reviews_host}/api/reviews", headers=self.headers, json=review)
        # search for movies by genre
        search_movie_by_genre_params = {
            'genre': 'DRAMA'
        }
        self.client.get(f"{self.movies_host}/api/movies/find", headers=self.headers, params=search_movie_by_genre_params)
        # get recommendations
        self.client.get(f"{self.recommendations_host}/api/recommendations/{movie_id}", headers=self.headers)
        # get top-rated movies
        self.client.get(f"{self.movies_host}/api/movies/user/{user_id}/favorite", headers=self.headers)
        # update rating for "Bastardi"
        review_update = {
            "movieId": movie_id,
            "userId": user_id,
            "id": math.floor(random.random() * 1000),
            "overallRating": 6.1,
        }
        self.client.put(f"{self.reviews_host}/api/reviews", headers=self.headers, json=review_update)
        # update list of favorite movies
        self.client.get(f"{self.movies_host}/api/movies/user/{user_id}/favorite", headers=self.headers)
        # logout

    """
        ## Scenario 2: new user usage
        1. User creates an account
        2. User logs in
        3. User explores the list of movies by genre
        4. User checks score of the Shawshank Redemption
        5. User ranks the Shawshank Redemption
        6. User logs out
    """
    @task
    def new_user_scenario(self):
        print("New user scenario")
        user_id = 2
        movie_id = 2
        # create account
        user = {
            "userName": "testUser" + str(random.randint(1, 1000)),
            "email": "testUser" + str(random.randint(1, 1000)) + "@gmail.com",
            "encryptedPassword": "testPassword",
            "userType": "USER"
        }
        self.client.post(f"{self.users_host}/api/users", headers=self.headers, json=user)
        # login mocked with on_start fn
        # explore movies by genre
        search_movie_by_genre_params = {
            'genre': 'DRAMA'
        }
        self.client.get(f"{self.movies_host}/api/movies/find", headers=self.headers,
                        params=search_movie_by_genre_params)
        # check score of the Shawshank Redemption
        self.client.get(f"{self.reviews_host}/api/reviews/{movie_id}", headers=self.headers)
        # rank the Shawshank Redemption
        review = {
            "movieId": movie_id,
            "userId": user_id,
            "id": math.floor(random.random() * 1000),
            "overallRating": 4.1,
        }
        self.client.post(f"{self.reviews_host}/api/reviews", headers=self.headers, json=review)
        # logout
