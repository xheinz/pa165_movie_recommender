import requests
from keycloak import KeycloakOpenID

# Configure client
keycloak_openid = KeycloakOpenID(server_url="http://localhost:8080",
                                 client_id="movie-recommender",
                                 realm_name="MoviesRecommender",
                                 client_secret_key="pMG3OYcz9zGOKtC9l75ubXmcTz34XpHC",
                                 verify=True)

# Obtain token
token = keycloak_openid.token("admin", "admin")
access_token = token['access_token']
print("Access token:", access_token)

# Define the base URL for the movies management service
movies_url = "http://localhost:8081/api/movies/find"

# Setup headers for HTTP request
headers = {
    'Authorization': f'Bearer {access_token}',
    'Content-Type': 'application/json'
}

print(">>>>>>>>>>>>>>>>>>>>>>>>>")
print(access_token)
print(">>>>>>>>>>>>>>>>>>>>>>>>>")

print("headers:", headers)
# Parameters for the request
params = {
    'id': 1  # Assuming you want to find the movie with ID 1
}

# Make a GET request to the find endpoint with parameters
response = requests.get(movies_url, headers=headers, params=params)

# Check response status and print results
if response.status_code == 200:
    print("Movies retrieved successfully:", response.json())
else:
    print("Failed to retrieve movies. Status code:", response.status_code, "Response:", response.text)



