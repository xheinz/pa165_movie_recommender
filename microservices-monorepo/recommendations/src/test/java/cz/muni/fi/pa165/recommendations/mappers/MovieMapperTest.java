package cz.muni.fi.pa165.recommendations.mappers;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class MovieMapperTest {

    private MovieMapper movieMapper;

    @BeforeEach
    public void setup() {
        movieMapper = Mappers.getMapper(MovieMapper.class);
    }

    @Test
    void mapToMovieDTO_ValidMovie_MapsCorrectly() {
        // Arrange
        Movie movie = TestDataFactory.createMovie(1L);

        // Act
        MovieDTO movieDTO = movieMapper.mapToMovieDTO(movie);

        // Assert
        assertDtoAndEntityEquality(movieDTO, movie);
    }

    @Test
    void mapToMovieDTOList_ValidMovieList_MapsCorrectly() {
        // Arrange
        int count = 5;
        List<Movie> movies = TestDataFactory.createMovieList(count);

        // Act
        List<MovieDTO> movieDTOs = movieMapper.mapToMovieDTOList(movies);

        // Assert
        assertThat(movieDTOs).isNotNull().hasSize(count);
        for (int i = 0; i < count; i++) {
            Movie movie = movies.get(i);
            MovieDTO movieDTO = movieDTOs.get(i);
            assertDtoAndEntityEquality(movieDTO, movie);
        }
    }

    private void assertDtoAndEntityEquality(MovieDTO movieDTO, Movie movie) {
        assertThat(movieDTO).isNotNull();
        assertThat(movieDTO.id()).isEqualTo(movie.getId());
        assertThat(movieDTO.name()).isEqualTo(movie.getName());
        assertThat(movieDTO.director()).isEqualTo(movie.getDirector());
        assertThat(movieDTO.genres()).isEqualTo(movie.getGenres());
        assertThat(movieDTO.actors()).isEqualTo(movie.getActors());
    }
}
