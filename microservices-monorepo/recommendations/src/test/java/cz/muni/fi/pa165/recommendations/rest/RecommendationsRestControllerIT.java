package cz.muni.fi.pa165.recommendations.rest;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.enums.MovieGenre;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.service.api.MoviesApiService;
import cz.muni.fi.pa165.recommendations.util.KeycloakTokenProvider;
import cz.muni.fi.pa165.recommendations.util.ObjectConveter;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class RecommendationsRestControllerIT {
    private static final long movieId = 1L;

    @MockBean
    private MoviesApiService moviesApiService;

    private String accessToken;

    @Autowired
    private KeycloakTokenProvider keycloakTokenProvider;

    @Autowired
    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }

    @Test
    void getRecommendedMovies_ValidId_SuccessAndRecommendations() throws Exception {
        // Arrange
        List<Movie> movies = TestDataFactory.createMovieList();
        List<MovieDTO> moviesDtos = TestDataFactory.createMovieDTOList();

        Mockito.when(moviesApiService.findById(movieId))
            .thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesApiService.getAllByGenres(Arrays.asList(MovieGenre.ACTION, MovieGenre.COMEDY)))
            .thenReturn(movies);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/recommendations/{id}", movieId)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        List<MovieDTO> response = ObjectConveter.convertJsonToListOfMovieDTO(responseJson);
        assertMovieDtosEquality(response, moviesDtos);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCount_SuccessAndRecommendations() throws Exception {
        // Arrange
        int count = 5;
        List<Movie> movies = TestDataFactory.createMovieList(5);
        List<MovieDTO> moviesDtos = TestDataFactory.createMovieDTOList(5);
        Mockito.when(moviesApiService.findById(movieId))
            .thenReturn(Optional.of(TestDataFactory.createMovie(movieId)));
        Mockito.when(moviesApiService.getAllByGenres(Arrays.asList(MovieGenre.ACTION, MovieGenre.COMEDY)))
            .thenReturn(movies);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders
                .get("/api/recommendations/{id}?count={count}", movieId, count)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        List<MovieDTO> response = ObjectConveter.convertJsonToListOfMovieDTO(responseJson);
        assertMovieDtosEquality(response, moviesDtos);
    }

    private void assertMovieDtosEquality(List<MovieDTO> result, List<MovieDTO> movieDTOs) {
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(movieDTOs.size());
        assertThat(result).containsAll(movieDTOs);
    }
}
