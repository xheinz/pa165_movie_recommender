package cz.muni.fi.pa165.recommendations.rest;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.facade.RecommendationsFacade;
import cz.muni.fi.pa165.recommendations.util.KeycloakTokenProvider;
import cz.muni.fi.pa165.recommendations.util.ObjectConveter;
import cz.muni.fi.pa165.recommendations.util.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {RecommendationsRestController.class})
public class RecommendationsRestControllerWebMvcIT {

    @Value("${info.app.oauth2.client-id}")
    private String clientId;

    @Value("${info.app.oauth2.client-secret}")
    private String clientSecret;

    @Value("${info.app.oauth2.username}")
    private String user;

    @Value("${info.app.oauth2.password}")
    private String password;

    @Value("${info.app.oauth2.realm}")
    private String realm;

    @Value("${info.app.oauth2.url}")
    private String url;

    private static final long movieId = 1L;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RecommendationsFacade recommendationsFacade;

    private String accessToken;

    private final KeycloakTokenProvider keycloakTokenProvider = new KeycloakTokenProvider();

    @BeforeEach
    public void setup() {
        keycloakTokenProvider.setClientId(clientId);
        keycloakTokenProvider.setClientSecret(clientSecret);
        keycloakTokenProvider.setUser(user);
        keycloakTokenProvider.setPassword(password);
        keycloakTokenProvider.setRealm(realm);
        keycloakTokenProvider.setUrl(url);
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }

    @Test
    void getRecommendedMovies_ValidId_SuccessAndRecommendations() throws Exception {
        // Arrange
        List<MovieDTO> movies = TestDataFactory.createMovieDTOList();
        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId))
            .thenReturn(movies);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/recommendations/{id}", movieId)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        List<MovieDTO> resultMovies = ObjectConveter.convertJsonToListOfMovieDTO(responseJson);
        assertEqualityOfDtos(movies, resultMovies);
        Mockito.verify(recommendationsFacade, Mockito.times(1)).getRecommendedMovies(movieId);
    }

    @Test
    void getRecommendedMovies_ValidIdSpecificCount_SuccessAndSpecificNumberOfRecommendations() throws Exception {
        // Arrange
        int count = 5;
        List<MovieDTO> movies = TestDataFactory.createMovieDTOList(count);
        Mockito.when(recommendationsFacade.getRecommendedMovies(movieId, count))
            .thenReturn(movies);

        // Act
        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/recommendations/{id}?count={count}",
                    movieId, count)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        // Assert
        List<MovieDTO> resultMovies = ObjectConveter.convertJsonToListOfMovieDTO(responseJson);
        assertEqualityOfDtos(movies, resultMovies);
        Mockito.verify(recommendationsFacade, Mockito.times(1)).getRecommendedMovies(movieId, count);
    }

//    @Test
//    void seekMyBeans() {
//        System.out.println("** My beans are **");
//        Arrays.stream(applicationContext.getBeanDefinitionNames())
//            .filter(beanName -> applicationContext.getBean(beanName)
//                .getClass()
//                .getPackageName()
//                .startsWith("cz.muni.fi.pa165")
//            )
//            .forEach(System.out::println);
//    }


    private void assertEqualityOfDtos(List<MovieDTO> result, List<MovieDTO> movieDTOs) {
        assertThat(result).isNotNull();
        assertThat(result.size()).isEqualTo(movieDTOs.size());
        assertThat(result).containsAll(movieDTOs);
    }
}
