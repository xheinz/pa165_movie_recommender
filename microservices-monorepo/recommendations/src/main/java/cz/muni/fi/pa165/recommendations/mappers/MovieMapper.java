package cz.muni.fi.pa165.recommendations.mappers;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MovieMapper {

	/**
	 * Maps movie to movie DTO
	 *
	 * @param movie movie
	 * @return movie DTO
	 */
	MovieDTO mapToMovieDTO(Movie movie);

	/**
	 * Maps list of movies to list of movie DTOs
	 *
	 * @param movies list of movies
	 * @return list of movie DTOs
	 */
	List<MovieDTO> mapToMovieDTOList(List<Movie> movies);

	/**
	 * Maps movie DTO to movie
	 *
	 * @param movieDTO movie DTO
	 * @return movie
	 */
	Movie mapToMovie(MovieDTO movieDTO);

	/**
	 * Maps list of movie DTOs to list of movies
	 *
	 * @param movieDTOs list of movie DTOs
	 * @return list of movies
	 */
	List<Movie> mapToMovieList(List<MovieDTO> movieDTOs);
}
