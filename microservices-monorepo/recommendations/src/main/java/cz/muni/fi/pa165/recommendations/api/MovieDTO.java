package cz.muni.fi.pa165.recommendations.api;

import cz.muni.fi.pa165.recommendations.data.enums.MovieGenre;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;

import java.util.List;

@Schema(title = "MovieDTO", description = "Represents a movie DTO")
public record MovieDTO(
		@NotBlank
		@Schema(description = "Movie id", example = "1544")
		Long id,

		@NotBlank
		@Schema(description = "Movie name", example = "The Shawshank Redemption")
		String name,

		@NotBlank
		@Schema(description = "Movie director", example = "Tom Hanks")
		String director,

		@NotEmpty
		@Schema(description = "Movie genres", example = "Action, Comedy, Drama")
		List<MovieGenre> genres,

		@NotEmpty
		@Schema(description = "Movie actors", example = "John Doe, Jane Doe, John Smith")
		List<String> actors,

		@Schema(description = "Movie's poster url", example = "https://picsum.photos/200")
		String imageUrl
) {
}
