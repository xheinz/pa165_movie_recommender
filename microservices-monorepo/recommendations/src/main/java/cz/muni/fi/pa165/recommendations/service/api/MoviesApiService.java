package cz.muni.fi.pa165.recommendations.service.api;

import cz.muni.fi.pa165.recommendations.api.MovieDTO;
import cz.muni.fi.pa165.recommendations.data.enums.MovieGenre;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.mappers.MovieMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;


@Service
public class MoviesApiService {
	private final WebClient client;
	private final MovieMapper movieMapper;

	/**
	 * Constructor for MoviesApiService.
	 * @param baseUrl The base URL of the movies management service.
	 * @param movieMapper The movie mapper.
	 */
	public MoviesApiService(@Value("${info.services.movies-management.url}") final String baseUrl,
							@Autowired MovieMapper movieMapper) {
		this.client = WebClient.create(baseUrl);
		this.movieMapper = movieMapper;
	}

	/**
	 * Returns all movies by genres.
	 *
	 * @param genres The genres to filter by.
	 * @return list of movies
	 */
	public List<Movie> getAllByGenres(List<MovieGenre> genres) {
		HashSet<Movie> movies = new HashSet<>();
		for (MovieGenre genre : genres) {
			List<Movie> moviesByGenre = client.get()
					.uri(uriBuilder -> uriBuilder
							.path("/api/movies/find")
							.queryParam("genre", genre)
							.build())
					.retrieve()
					.bodyToFlux(MovieDTO.class)
					.map(movieMapper::mapToMovie)
					.collectList()
					.block();
			if (moviesByGenre != null) {
				movies.addAll(moviesByGenre);
			}
		}
		return new ArrayList<>(movies);
	}

	/**
	 * Returns movie by its id
	 *
	 * @param id movie id
	 * @return movie
	 */
	public Optional<Movie> findById(final Long id) {
		return client.get()
				.uri(uriBuilder -> uriBuilder
						.path("/api/movies/find")
						.queryParam("id", id)
						.build())
				.retrieve()
				.bodyToMono(MovieDTO.class)
				.map(movieMapper::mapToMovie)
				.map(Optional::of)
				.defaultIfEmpty(Optional.empty())
				.block();
	}
}
