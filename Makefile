.PHONY: help commit-acceptance compile test verify pmd checkstyle spotbugs clean

CACHE ?= ./.cache
REPO ?= ./microservices-monorepo

MVN = mvn -f $(REPO)
PMD = $(CACHE)/pmd-bin-7.0.0/bin/pmd
PMD_CHECK_DIRS = $(REPO)/movies-management/src,$(REPO)/ratings-and-reviews/src,$(REPO)/recommendations/src,$(REPO)/user-management/src

define log
    @echo "================================== $(1) ==================================="
endef

help:  ## Print help message
	@printf "Usage: make <command>\n\n"
	@printf "Commands:\n"
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

commit-acceptance: clean compile checkstyle spotbugs  ## Run build and pre-commit linters/formatters

compile test verify: clean  ## Run maven compile/test/verify targets
	$(call log, "Running maven $@ target")
	$(MVN) $@

$(PMD):  ## Download PMD-7.0.0
	$(call log, "Downloading PMD-7.0.0 from github releases...")
	curl -Ls https://github.com/pmd/pmd/releases/download/pmd_releases%2F7.0.0/pmd-dist-7.0.0-bin.zip --create-dirs -o $(CACHE)/pmd-dist-7.0.0-bin.zip
	unzip -qo $(CACHE)/pmd-dist-7.0.0-bin.zip -d $(CACHE)

pmd: $(PMD) compile  ## Run PMD
	$(call log, "Running PMD")
	$(PMD) check --cache $(CACHE)/pmd-cache -R $(REPO)/resources/pmd-rules.xml -d $(PMD_CHECK_DIRS)

spotbugs:  ## Run SpotBugs
checkstyle:  ## Run checkstyle
spotbugs checkstyle: compile
	$(call log, "Running $@")
	$(MVN) --fail-at-end $@:check

clean:  ## Run clean maven target
	$(call log, "Running maven clean target")
	$(MVN) clean
