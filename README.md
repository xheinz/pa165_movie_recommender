# PA165_movie_recommender

## Project Description

The web application is a catalogue of movies of different genres. Each movie has a title, description, film director,
and list of actors, and image(s) from the main playbill. Each movie is categorized in one or more genres. Only the
adfministrator can create, remove and update the list of movies. Users can rate the movies according to different
criteria (e.g how novel are the ideas of the movie, their final score, etc…). The main feature of the system is that
users can pick one movie and get the list of similar movies and / or movies that were liked the most by other users
watching the same movie (no need of complex algorithms, some simple recommendation is enough!).

## Usage
To run the application with docker-compose:
```shell
docker-compose up -d
```

## Tests
Execute unit tests:
```shell
make test
```
**Integration tests require keycloak running on localhost:8080.** 
To start the keycloak container with docker-compose, run inside microservices directory:
```shell
docker-compose up keycloak -d
```
Then you can execute integration tests:
```shell
make verify
```

## Docker Compose

to run the docker compose command:

- first you need to define your own custom .env file containing passwords of your choice that you can use to login to
  keycloak later, there is already a file called .env.example provided as an example on how it should look
- also make sure that you have used maven clean install or maven package to generate all the target files that are
  necessary for the creation of the docker containers

```
cd microservices-monorepo
docker-compose up -d
```

## Security:

#### Keycloak:

- you need to first define the .env file mentioned above, then once you have the containers running, go to keycloak
  address - http://localhost:8080/ and login with your credentials
- the keycloak data is physically mapped to this repository so the realm is already created and configured

## Showcase scenarios

### Scenario 1: experienced user usage
1. User logs in
2. User rates a recently watched movie.
3. User searches for movies by genre, looking for something similar to the last movie watched
4. Not finding anything appealing, the user turns to the recommendation system.
5. Still not finding anything of interest, the user reviews his list of top-rated movies.
6. User updates the rating for "Bastardi," marking it lower due to a change in opinion since first viewing it at a younger age
7. The user's list of favorite movies is updated, and "Bastardi" is no longer included
8. User logs out.

### Scenario 2: New user looks around
1. User creates an account
2. User logs in
3. User explores the list of movies by genre
4. User checks score of the Shawshank Redemption
5. User ranks the Shawshank Redemption
6. User logs out

## Microservices:

#### 1. Movies Management Service

Handles CRUD operations for movies, including title, description, film director, actors, images, and genres. This
service is used by the administrator for managing the movie catalogue.

##### Seeding the database
The service makes possible to initialize and clear the database conditionally using Spring Profiles. 
The `db-init` profile initializes the database with some mock movie data. 
The `db-clear` profile clears all data from the database. 
These profiles can be activated by specifying them in the spring.profiles.active property in the application.yml file
or as a command-line argument when starting the application.

#### 2. User Management and Authentication Service

Manages user accounts, authentication, and authorization. This ensures that only administrators have the ability to
modify the movie catalogue, while users can rate movies and view recommendations.

#### 3. Ratings and Reviews Service

Handles the storage, retrieval, and processing of movie ratings and reviews according to different criteria. This
service aggregates user ratings to provide insights and data for recommendations.

#### 4. Recommendation Service

Provides movie recommendations based on simple algorithms, such as movies with similar genres or high ratings from users
who liked the same movies. This service can use the data from the Ratings and Reviews Service to generate
recommendations.

## Use case diagram

![Use case Diagram](docs/diagrams/useCase-diagram.png)

## Class diagram for DTO's

![DTO Class Diagram](docs/diagrams/dto-class-diagram.png)
